/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.plotter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.core.config.Configurable;
import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.core.config.update.FileSystemUpdater;
import org.xsts.core.plotters.Plottable;

import java.io.File;
import java.io.IOException;

public abstract class BasicPdfPlotterProto implements Plottable {

    public abstract String fileName();
    public abstract void processPages(PDRectangle rect, PDDocument doc) throws IOException;
    public abstract void updateMetaInformation(PDDocument doc);

    @Override
    public void process() {
        Configurable config = EnvironmentConfiguration.getInstance();
        File pdfFile = new File(config.getString(FileSystemUpdater.DATA_OUTPUT_DIR),
                fileName());

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(100,100);
            PDPage page = null;
            PDPageContentStream contentStream = null;

            processPages(rect, doc);
            updateMetaInformation(doc);
            doc.save(pdfFile);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
