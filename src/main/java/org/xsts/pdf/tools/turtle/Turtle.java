/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 * The turtle package has been adapted from code available on the internet
 * Be mindful that this version of the turtle implements ONLY A SUBSET of the VERBS of the classical LOGO framework.
 * For more information see the tutorials
 */
package org.xsts.pdf.tools.turtle;

import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.xsts.pdf.tools.floatval.PTFLine;

import java.awt.*;
import java.io.IOException;

public class Turtle {
    PDPageContentStream contentStream;
    TurtleData data;

    PTFLine plotter;

    public Turtle(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
        data = new TurtleData();
        plotter = new PTFLine(contentStream);
        plotter.width(0.01f);
        plotter.color(Color.black);
    }

    public Turtle setPosition(double xPos, double yPos) {
        data.position(xPos, yPos);
        return this;
    }

    public Turtle setRotation(double angle) {
        data.angle(angle);
        return this;
    }

    public Turtle move(double d) {
        data.move(d);
        return this;
    }

    public void forward(double d) throws IOException {
        double deltax = data.horizontalProjection * d;
        double deltay = data.verticalProjection * d;
        plotter
                .from((float) data.abscissa,(float) data.ordinate)
                .to((float)(data.abscissa +deltax), (float)(data.ordinate +deltay))
                .draw();
        data.offset(deltax, deltay);
    }

    public void backward(double d) throws IOException {
        forward(-d);
    }

    public void resize(double s) {
        data.scale(s);
    }



    public void left(double alpha) {
        data.turn(alpha);
    }

    public void right(double alpha) {
        data.turn(-alpha);
    }
}
