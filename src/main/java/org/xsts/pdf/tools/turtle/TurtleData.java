/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 * The turtle package has been adapted from code available on the internet
 * Be mindful that this version of the turtle has ONLY A SUBSET of the classical LOGO framework
 * For more information see the tutorials
 */
package org.xsts.pdf.tools.turtle;

class TurtleData {
    double abscissa;
    double ordinate;
    double horizontalProjection;
    double verticalProjection;
    double radius;
    double angle;

    TurtleData() {
        abscissa = 0.0;
        ordinate = 0.0;
        radius = 1.0;
        angle = 0.0;
        horizontalProjection = 1.0;
        verticalProjection = 0.0;
    }

    public void position(double xPos, double yPos) {
        abscissa = xPos;
        ordinate = yPos;
    }

    public void angle(double newAngle) {
        angle = newAngle;
        horizontalProjection = radius * Math.cos(angle);
        verticalProjection = radius * Math.sin(angle);
    }

    public void turn(double alpha)  {
        angle += alpha;
        horizontalProjection = radius * Math.cos(angle);
        verticalProjection = radius * Math.sin(angle);
    }

    public void move(double d) {
        double deltax = horizontalProjection * d;
        double deltay = verticalProjection * d;
        abscissa += deltax;
        ordinate += deltay;
    }

    public void offset(double deltax, double deltay) {
        abscissa += deltax;
        ordinate += deltay;
    }

    public void scale(double scaleFactor) {
        radius *= scaleFactor;
        horizontalProjection = radius * Math.cos(angle);
        verticalProjection = radius * Math.sin(angle);
    }
}
