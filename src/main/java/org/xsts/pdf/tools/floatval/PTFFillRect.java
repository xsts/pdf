/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.floatval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.*;
import java.io.IOException;

public class PTFFillRect {
    PDPageContentStream contentStream = null;
    float x = 0;
    float y = 0;
    float width = 0;
    float height = 0;
    float borderWidth = 0;
    Color fillColor = null;
    Color borderColor = null;

    public PTFFillRect(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTFFillRect origin(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public PTFFillRect dimensions(float width, float height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public PTFFillRect borderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public PTFFillRect fillColor(Color fillColor) {
        this.fillColor = fillColor;
        return this;
    }

    public PTFFillRect borderColor(Color borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public PTFFillRect draw() throws IOException {
        contentStream.saveGraphicsState();
        if ( borderWidth > 0){
            contentStream.setStrokingColor(borderColor);
            contentStream.setLineWidth(borderWidth);
            contentStream.setLineJoinStyle(0);
            contentStream.setLineCapStyle(0);

            contentStream.moveTo(x,y);
            contentStream.lineTo(x,y + height);
            contentStream.lineTo(x+width,y + height);
            contentStream.lineTo(x+width,y);
            contentStream.lineTo(x,y);
            contentStream.lineTo(x,y + height);
            contentStream.stroke();
        }

        contentStream.setNonStrokingColor(fillColor);
        contentStream.addRect(x,y,width,height);
        contentStream.fill();


        contentStream.restoreGraphicsState();
        return this;
    }
}
