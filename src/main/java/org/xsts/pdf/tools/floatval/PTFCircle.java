/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.floatval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.*;
import java.io.IOException;

public class PTFCircle {
    PDPageContentStream contentStream = null;
    float x = 0;
    float y = 0;
    float radius = 0;
    float borderWidth = 0;
    Color borderColor = null;

    public PTFCircle(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTFCircle center(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public PTFCircle radius(float radius) {
        this.radius = radius;
        return this;
    }

    public PTFCircle borderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public PTFCircle borderColor(Color borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public PTFCircle draw() throws IOException {

        contentStream.saveGraphicsState();
        final float k = 0.552284749831f;
        contentStream.setNonStrokingColor(borderColor);
        contentStream.setLineWidth(borderWidth);
        contentStream.moveTo(x - radius, y);
        contentStream.curveTo(x - radius, y + k * radius, x - k * radius, y + radius, x, y + radius);
        contentStream.curveTo(x + k * radius, y + radius, x + radius, y + k * radius, x + radius, y);
        contentStream.curveTo(x + radius, y - k * radius, x + k * radius, y - radius, x, y - radius);
        contentStream.curveTo(x - k * radius, y - radius, x - radius, y - k * radius, x - radius, y);
        contentStream.closeAndStroke();
        contentStream.restoreGraphicsState();

        return this;
    }
}
