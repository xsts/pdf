/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.floatval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PTFPath {
    PDPageContentStream contentStream = null;
    List<Float> xCoord = new ArrayList<>();
    List<Float> yCoord = new ArrayList<>();
    float width = 0;
    int capStyle = 0;
    int joinStyle = 0;
    Color color = null;

    public PTFPath(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTFPath from(float x, float y) {
        xCoord = new ArrayList<>();
        xCoord = new ArrayList<>();
        xCoord.add(x);
        yCoord.add(y);
        return this;
    }

    public PTFPath to(float x, float y) {
        xCoord.add(x);
        yCoord.add(y);
        return this;
    }

    public PTFPath width(float width) {
        this.width = width;
        return this;
    }

    public PTFPath color(Color color) {
        this.color = color;
        return this;
    }

    public PTFPath capStyle(int capStyle) {
        this.capStyle = capStyle;
        return this;
    }

    public PTFPath joinStyle(int joinStyle) {
        this.joinStyle = joinStyle;
        return this;
    }

    public PTFPath draw() throws IOException {
        contentStream.saveGraphicsState();
        contentStream.setStrokingColor(color);
        contentStream.setLineWidth(width);
        contentStream.setLineJoinStyle(joinStyle);
        contentStream.setLineCapStyle(capStyle);


        for ( int i = 1; i < xCoord.size(); i++){
            contentStream.moveTo(xCoord.get(i-1), yCoord.get(i-1));
            contentStream.lineTo(xCoord.get(i), yCoord.get(i));
            contentStream.stroke();

        }

        //float[] x = ArrayUtils.toPrimitive(xCoord.toArray(new Float[0]), 0.0F);
        //float[] y = ArrayUtils.toPrimitive(yCoord.toArray(new Float[0]), 0.0F);

        //contentStream.drawPolygon(x,y);

        contentStream.restoreGraphicsState();
        return this;
    }
}
