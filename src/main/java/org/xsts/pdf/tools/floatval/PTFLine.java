/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.floatval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.Color;
import java.io.IOException;

public class PTFLine {
    PDPageContentStream contentStream = null;
    float x1 = 0;
    float y1 = 0;
    float x2 = 0;
    float y2 = 0;
    float width = 0;
    int capStyle = 0;
    int joinStyle = 0;
    Color color = null;

    public PTFLine(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTFLine from(float x, float y) {
        x1 = x;
        y1 = y;
        return this;
    }

    public PTFLine to(float x, float y) {
        x2 = x;
        y2 = y;
        return this;
    }

    public PTFLine width(float width) {
        this.width = width;
        return this;
    }

    public PTFLine color(Color color) {
        this.color = color;
        return this;
    }

    public PTFLine capStyle(int capStyle) {
        this.capStyle = capStyle;
        return this;
    }

    public PTFLine joinStyle(int joinStyle) {
        this.joinStyle = joinStyle;
        return this;
    }


    public PTFLine draw() throws IOException {
        contentStream.saveGraphicsState();
        contentStream.setStrokingColor(color);
        contentStream.setLineWidth(width);
        contentStream.setLineJoinStyle(joinStyle);
        contentStream.setLineCapStyle(capStyle);
        contentStream.moveTo(x1,y1);
        contentStream.lineTo(x2,y2);
        contentStream.stroke();
        contentStream.restoreGraphicsState();
        return this;
    }
}
