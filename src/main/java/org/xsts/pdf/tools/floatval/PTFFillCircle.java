/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.floatval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.*;
import java.io.IOException;

public class PTFFillCircle {
    PDPageContentStream contentStream = null;
    float x = 0;
    float y = 0;
    float radius = 0;
    float borderWidth = 0;
    Color borderColor = null;
    Color fillColor = null;


    public PTFFillCircle(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTFFillCircle center(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public PTFFillCircle radius(float radius) {
        this.radius = radius;
        return this;
    }

    public PTFFillCircle borderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public PTFFillCircle borderColor(Color borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public PTFFillCircle fillColor(Color fillColor) {
        this.fillColor = fillColor;
        return this;
    }

    public PTFFillCircle draw() throws IOException {

        contentStream.saveGraphicsState();
        final float k = 0.552284749831f;
        contentStream.saveGraphicsState();
        if (fillColor != null){
            contentStream.setNonStrokingColor(fillColor);
            contentStream.setLineWidth(0);
            contentStream.moveTo(x - radius, y);
            contentStream.curveTo(x - radius, y + k * radius, x - k * radius, y + radius, x, y + radius);
            contentStream.curveTo(x + k * radius, y + radius, x + radius, y + k * radius, x + radius, y);
            contentStream.curveTo(x + radius, y - k * radius, x + k * radius, y - radius, x, y - radius);
            contentStream.curveTo(x - k * radius, y - radius, x - radius, y - k * radius, x - radius, y);
            contentStream.fill();
        }
        contentStream.restoreGraphicsState();

        contentStream.saveGraphicsState();
        if ( borderColor != null) {
            contentStream.setStrokingColor(borderColor);
            contentStream.setLineWidth(borderWidth);
            contentStream.moveTo(x - radius, y);
            contentStream.curveTo(x - radius, y + k * radius, x - k * radius, y + radius, x, y + radius);
            contentStream.curveTo(x + k * radius, y + radius, x + radius, y + k * radius, x + radius, y);
            contentStream.curveTo(x + radius, y - k * radius, x + k * radius, y - radius, x, y - radius);
            contentStream.curveTo(x - k * radius, y - radius, x - radius, y - k * radius, x - radius, y);
            contentStream.closeAndStroke();
        }
        contentStream.restoreGraphicsState();

        contentStream.restoreGraphicsState();

        return this;
    }
}
