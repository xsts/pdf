/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.floatval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.util.Matrix;

import java.awt.*;
import java.io.IOException;

public class PTFText {
    public static final int LEFT = 0;
    public static final int CENTER = 1;
    public static final int RIGHT = 2;


    PDPageContentStream contentStream = null;
    float x = 0;
    float y = 0;
    Color color = null;
    PDFont font = null;
    float size = 0;
    String text;
    float angle = 0;
    int position = LEFT;

    public PTFText(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTFText at(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public PTFText color(Color color) {
        this.color = color;
        return this;
    }

    public PTFText text(String  text) {
        /*
        XstsConfiguration xstsConfig = XstsConfiguration.getInstance();

        if ( xstsConfig.hasProperty(TranslationUpdater.TRANSLATOR)) {
            GlobalTranslator translator = (GlobalTranslator) xstsConfig.get(TranslationUpdater.TRANSLATOR);
            this.text = translator.translate(text);
        } else {
            this.text = text;
        }*/
        this.text = text;

        return this;
    }

    public PTFText position(int position) {
        if ( position != LEFT && position != CENTER && position != RIGHT)
            this.position = LEFT;
        else
            this.position = position;
        return this;
    }

    public PTFText angle(float angle) {
        this.angle = angle;
        return this;
    }

    public PTFText size(float size) {
        this.size = size;
        return this;
    }

    public PTFText font(PDFont font) {
        this.font = font;
        return this;
    }

    public PTFText draw() throws IOException {

        contentStream.saveGraphicsState();



        float textSize = font.getStringWidth(text); // in thousandths of font pt size.
        float stringWidth = font.getStringWidth( text );



        float th = (float)(font.getFontDescriptor().getFontBoundingBox().getHeight() * size / 1000);
        float tw = (float)(stringWidth * size / 1000);
        float ta = (float)(font.getFontDescriptor().getAscent() * size / 1000);
        float td = (float)(font.getFontDescriptor().getDescent() * size / 1000);
        float tad = (float)((font.getFontDescriptor().getAscent() -font.getFontDescriptor().getDescent())
                * size / 1000);

        Matrix mat = new Matrix();
        float alfa = (float)(angle*Math.PI/180.0);

        float lead=  (float)(font.getFontDescriptor().getLeading() * size / 1000);
        float tm = tad/2; //ta + td; //(ta+td)/2 ;
        switch(position) {
            case CENTER:
                mat.translate((float)(x+tm*Math.sin(alfa) -((tw/2))*(Math.cos(alfa))),
                        (float)(y-tm*Math.cos(alfa) -((tw/2))*(Math.sin(alfa))));
                break;
            case RIGHT:
                mat.translate((float)(x+tm*Math.sin(alfa) -(tw)*(Math.cos(alfa))),
                        (float)(y-tm*Math.cos(alfa)-((tw))*(Math.sin(alfa))));
                break;
            case LEFT:
            default:
                mat.translate((float)(x),
                        (float)(y-tm));

                break;
        }
        mat.rotate(alfa);





        contentStream.beginText();
        contentStream.setTextMatrix(mat);

        contentStream.setStrokingColor(color);
        contentStream.setNonStrokingColor(color);
        contentStream.setFont(font, size);
        contentStream.showText(text);
        contentStream.endText();

        contentStream.restoreGraphicsState();


        return this;
    }
}
