/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.intval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.util.Matrix;

import java.awt.Color;
import java.io.IOException;

public class PTIText {

    public static final int LEFT = 0;
    public static final int CENTER = 1;
    public static final int RIGHT = 2;


    PDPageContentStream contentStream = null;
    int x = 0;
    int y = 0;
    Color color = null;
    PDFont font = null;
    int size = 0;
    String text;
    int angle = 0;
    int position = LEFT;

    public PTIText(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTIText at(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public PTIText color(Color color) {
        this.color = color;
        return this;
    }

    public PTIText text(String  text) {
        this.text = text;
        return this;
    }

    public PTIText position(int position) {
        if ( position != LEFT && position != CENTER && position != RIGHT)
            this.position = LEFT;
        else
            this.position = position;
        return this;
    }

    public PTIText angle(int angle) {
        this.angle = angle;
        return this;
    }

    public PTIText size(int size) {
        this.size = size;
        return this;
    }

    public PTIText font(PDFont font) {
        this.font = font;
        return this;
    }

    public PTIText draw() throws IOException {

        contentStream.saveGraphicsState();

        float textSize = font.getStringWidth(text); // in thousandths of font pt size.
        float stringWidth = font.getStringWidth( text );

        int th = (int)(font.getFontDescriptor().getFontBoundingBox().getHeight() * size / 1000);
        int tw = (int)(stringWidth * size / 1000);
        int ta = (int)(font.getFontDescriptor().getAscent() * size / 1000);
        int td = (int)(font.getFontDescriptor().getDescent() * size / 1000);
        int tad = (int)((font.getFontDescriptor().getAscent() -font.getFontDescriptor().getDescent())
                * size / 1000);
        System.out.println("height = " + th);
        System.out.println("ascent = " + ta);
        System.out.println("descent = " + td);
        Matrix mat = new Matrix();
        float alfa = (float)(angle*Math.PI/180.0);

        int lead=  (int)(font.getFontDescriptor().getLeading() * size / 1000);
        int tm = tad/2; //ta + td; //(ta+td)/2 ;
        switch(position) {
            case CENTER:
                mat.translate((float)(x+tm*Math.sin(alfa) -((tw/2))*(Math.cos(alfa))),
                        (float)(y-tm*Math.cos(alfa) -((tw/2))*(Math.sin(alfa))));
                break;
            case RIGHT:
                mat.translate((float)(x+tm*Math.sin(alfa) -(tw)*(Math.cos(alfa))),
                        (float)(y-tm*Math.cos(alfa)-((tw))*(Math.sin(alfa))));
                break;
            case LEFT:
            default:
                mat.translate((float)(x),
                        (float)(y-tm));

                break;
        }
        mat.rotate(alfa);

        contentStream.beginText();
        contentStream.setTextMatrix(mat);

        contentStream.setStrokingColor(color);
        contentStream.setNonStrokingColor(color);
        contentStream.setFont(font, size);
        contentStream.showText(text);
        contentStream.endText();

        contentStream.restoreGraphicsState();

        return this;
    }
}
