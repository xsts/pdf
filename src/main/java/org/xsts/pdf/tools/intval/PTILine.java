/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.intval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.Color;
import java.io.IOException;

public class PTILine {

    PDPageContentStream contentStream = null;
    int x1 = 0;
    int y1 = 0;
    int x2 = 0;
    int y2 = 0;
    int width = 0;
    int capStyle = 0;
    int joinStyle = 0;
    Color color = null;

    public PTILine(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTILine from(int x, int y) {
        x1 = x;
        y1 = y;
        return this;
    }

    public PTILine to(int x, int y) {
        x2 = x;
        y2 = y;
        return this;
    }

    public PTILine width(int width) {
        this.width = width;
        return this;
    }

    public PTILine color(Color color) {
        this.color = color;
        return this;
    }

    public PTILine capStyle(int capStyle) {
        this.capStyle = capStyle;
        return this;
    }

    public PTILine joinStyle(int joinStyle) {
        this.joinStyle = joinStyle;
        return this;
    }

    public PTILine draw() throws IOException {
        contentStream.saveGraphicsState();
        contentStream.setStrokingColor(color);
        contentStream.setLineWidth(width);
        contentStream.setLineJoinStyle(joinStyle);
        contentStream.setLineCapStyle(capStyle);
        contentStream.moveTo(x1,y1);
        contentStream.lineTo(x2,y2);
        contentStream.stroke();
        contentStream.restoreGraphicsState();
        return this;
    }
}
