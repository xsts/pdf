/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.intval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.Color;
import java.io.IOException;

public class PTICircle {

    PDPageContentStream contentStream = null;
    int x = 0;
    int y = 0;
    int radius = 0;
    int borderWidth = 0;
    Color borderColor = null;

    public PTICircle(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTICircle center(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public PTICircle radius(int radius) {
        this.radius = radius;
        return this;
    }

    public PTICircle borderWidth(int borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public PTICircle borderColor(Color borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public PTICircle draw() throws IOException {
        contentStream.saveGraphicsState();
        final float k = 0.552284749831f;
        contentStream.setNonStrokingColor(borderColor);
        contentStream.setLineWidth(borderWidth);
        contentStream.moveTo(x - radius, y);
        contentStream.curveTo(x - radius, y + k * radius, x - k * radius, y + radius, x, y + radius);
        contentStream.curveTo(x + k * radius, y + radius, x + radius, y + k * radius, x + radius, y);
        contentStream.curveTo(x + radius, y - k * radius, x + k * radius, y - radius, x, y - radius);
        contentStream.curveTo(x - k * radius, y - radius, x - radius, y - k * radius, x - radius, y);
        contentStream.closeAndStroke();
        contentStream.restoreGraphicsState();

        return this;
    }
}
