
/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.intval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.Color;
import java.io.IOException;

public class PTIRect {
    PDPageContentStream contentStream = null;
    int x = 0;
    int y = 0;
    int width = 0;
    int height = 0;
    int borderWidth = 0;
    Color borderColor = null;

    public PTIRect(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTIRect origin(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public PTIRect dimensions(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public PTIRect borderWidth(int borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public PTIRect borderColor(Color borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public PTIRect draw() throws IOException {
        contentStream.saveGraphicsState();
        contentStream.setStrokingColor(borderColor);
        contentStream.setLineWidth(borderWidth);
        contentStream.setLineJoinStyle(0);
        contentStream.setLineCapStyle(0);

        contentStream.moveTo(x,y);
        contentStream.lineTo(x,y + height);
        contentStream.lineTo(x+width,y + height);
        contentStream.lineTo(x+width,y);
        contentStream.lineTo(x,y);
        contentStream.lineTo(x,y + height);
        contentStream.stroke();

        contentStream.restoreGraphicsState();
        return this;
    }
}
