/*
 * Group : XSTS
 * Project : Simple PDF library for XSTS printables
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.pdf.tools.intval;

import org.apache.pdfbox.pdmodel.PDPageContentStream;

import java.awt.Color;
import java.io.IOException;

public class PTIFillRect {
    PDPageContentStream contentStream = null;
    int x = 0;
    int y = 0;
    int width = 0;
    int height = 0;
    int borderWidth = 0;
    Color fillColor = null;
    Color borderColor = null;

    public PTIFillRect(PDPageContentStream contentStream) {
        this.contentStream = contentStream;
    }

    public PTIFillRect origin(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public PTIFillRect dimensions(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public PTIFillRect borderWidth(int borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public PTIFillRect fillColor(Color fillColor) {
        this.fillColor = fillColor;
        return this;
    }

    public PTIFillRect borderColor(Color borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public PTIFillRect draw() throws IOException {
        contentStream.saveGraphicsState();
        if ( borderWidth > 0){
            contentStream.setStrokingColor(borderColor);
            contentStream.setLineWidth(borderWidth);
            contentStream.setLineJoinStyle(0);
            contentStream.setLineCapStyle(0);

            contentStream.moveTo(x,y);
            contentStream.lineTo(x,y + height);
            contentStream.lineTo(x+width,y + height);
            contentStream.lineTo(x+width,y);
            contentStream.lineTo(x,y);
            contentStream.lineTo(x,y + height);
            contentStream.stroke();
        }

        contentStream.setNonStrokingColor(fillColor);
        contentStream.addRect(x,y,width,height);
        contentStream.fill();


        contentStream.restoreGraphicsState();
        return this;
    }
}
